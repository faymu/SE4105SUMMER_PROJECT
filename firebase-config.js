import * as firebase from 'firebase'
import 'firebase/firestore'

firebase.initializeApp({
  apiKey: 'AIzaSyC8GobUMMQGMMfEm712PCI7TOA484bKFu8',
  authDomain: 'sitterc-d5e5d.firebaseapp.com',
  databaseURL: 'https://sitterc-d5e5d.firebaseio.com',
  projectId: 'sitterc-d5e5d',
  storageBucket: 'sitterc-d5e5d.appspot.com',
  messagingSenderId: '807595466714'
})

const db = firebase.firestore()
const auth = firebase.auth()
db.settings({timestampsInSnapshots: true})
export { db, auth }
