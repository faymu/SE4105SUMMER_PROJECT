export default (state = {}, action) => {
  switch (action.type) {
    case 'ADD_TRANSACTION':
      return {
        ...state
      }
    case 'GET_PARENT_TRANSACTIONS':
      return {
        ...state,
        parentTransactions: action.payload
      }
    case 'GET_SITTER_TRANSACTIONS':
      return {
        ...state,
        sitterTransactions: action.payload
      }
    default:
      return state
  }
}
