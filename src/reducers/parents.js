export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_PARENT':
      return {
        ...state,
        parent: action.payload
      }
    case 'ADD_PARENT':
      return {
        ...state,
        addedParent: action.payload
      }
    default:
      return state
  }
}
