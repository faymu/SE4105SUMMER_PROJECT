export default (state = {}, action) => {
  switch (action.type) {
    case 'ADD_REQUEST':
      return {
        ...state
      }
    case 'GET_PENDING_JOB_REQUESTS':
      return {
        ...state,
        pendingJobRequests: action.payload
      }
    case 'GET_APPROVED_JOB_REQUESTS':
      return {
        ...state,
        approvedJobRequests: action.payload
      }
    case 'GET_SITTER_PENDING_REQUESTS':
      return {
        ...state,
        pendingSitterRequests: action.payload
      }
    case 'GET_SITTER_APPROVED_REQUESTS':
      return {
        ...state,
        approvedSitterRequests: action.payload
      }
    default:
      return state
  }
}
