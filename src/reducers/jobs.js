export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_JOBS':
      return {
        ...state,
        jobs: action.payload
      }
    case 'GET_USER_ADDED_JOBS':
      return {
        ...state,
        userAddedJobs: action.payload
      }
    case 'ADD_JOB':
      return {
        ...state,
        addedJob: action.payload
      }
    default:
      return state
  }
}
