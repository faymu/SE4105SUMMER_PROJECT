export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_SITTER':
      return {
        ...state,
        sitters: action.payload
      }
    case 'ADD_SITTER':
      return {
        ...state,
        addedSitter: action.payload
      }
    case 'GET_SITTER_BY_ID':
      return {
        ...state,
        sitter: action.payload
      }
    default:
      return state
  }
}
