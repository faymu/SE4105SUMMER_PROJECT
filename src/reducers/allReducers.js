import sitters from '../reducers/sitters'
import parents from '../reducers/parents'
import jobs from '../reducers/jobs'
import requests from '../reducers/requests'
import transactions from '../reducers/transactions'
import login from '../reducers/login'
import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

export default combineReducers({
  sitters: sitters,
  parents: parents,
  jobs: jobs,
  requests: requests,
  transactions: transactions,
  login: login,
  form: formReducer
})
