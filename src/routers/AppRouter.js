import React from 'react'
import { Route, Router , Switch } from 'react-router-dom'
import Home from '../components/Home'
import Login from '../components/LoginPage'
import SitterDashboard from '../containers/JobContainer'
import ParentDashboard from '../containers/ParentContainer'
import Bookings from '../containers/SitterRequestContainer'
import ParentTransactions from '../containers/ParentTransactionsContainer'
import SitterTransactions from '../containers/SitterTransactionsContainer'
import SitterSignUpPage from '../components/sitter/SitterSignUpPage'
import ParentSignUpPage from '../components/parent/ParentSignUpPage'
import AddJobPage from '../components/parent/AddJobPage'
import Jobs from '../containers/UserAddedJobsContainer'
import Error404 from '../components/Error404'
import history from '../history'
import Requests from '../containers/RequestContainer'

const AppRouter = () => (
  <Router history = { history }>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/login' component={Login}/>
      <Route path='/sitter/dashboard' component={SitterDashboard}/>
      <Route path='/parent/dashboard' component={ParentDashboard}/>
      <Route path='/sitter/signup' component={SitterSignUpPage}/>
      <Route path='/parent/signup' component={ParentSignUpPage}/>
      <Route path='/parent/jobs' component={Jobs}/>
      <Route path='/parent/job/:jobId' component={Requests}/>
      <Route path='/parent/addjob' component={AddJobPage}/>
      <Route path='/sitter/bookings' component={Bookings}/>
      <Route path='/parent/transactions' component={ParentTransactions}/>
      <Route path='/sitter/transactions' component={SitterTransactions}/>
      <Route component={Error404}/>
    </Switch>
  </Router>
)

export default AppRouter
