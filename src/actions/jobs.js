import { addJobToDb, fetchJobs, fetchFilteredJobs, fetchJobsAddedByUser, deletePostedJob } from '../handlers/jobHandler'
import history from '../history'

export const addJob = (job) => {
  return async (dispatch) => {
    await addJobToDb(job)
    dispatch({ type: 'ADD_JOB', payload: job })
  }
}

export const getAllJobs = () => {
  return async (dispatch) => {
    const jobs = await fetchJobs()
    dispatch({ type: 'GET_JOBS', payload: jobs })
  }
}

export const getFilteredJobs = (filter) => {
  return async (dispatch) => {
    const jobs = await fetchFilteredJobs(filter)
    if (jobs) {
      dispatch({ type: 'GET_JOBS', payload: jobs })
    }
  }
}

export const getUserAddedJobs = () => {
  return async (dispatch) => {
    const jobs = await fetchJobsAddedByUser()
    if (jobs) {
      dispatch({ type: 'GET_USER_ADDED_JOBS', payload: jobs })
    }
  }
}

export const deleteJob = (jobId) => {
  return async (dispatch) => {
    await deletePostedJob(jobId)
    dispatch({ type: 'DELETE_JOB' })
  }
}
