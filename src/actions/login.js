import { logInWithEmailAndPassword } from '../handlers/loginHandler'

export const logIn = (user) => {
  return async (dispatch) => {
    logInWithEmailAndPassword(user)
    dispatch({ type: 'LOGIN', payload: user })
  }
}
