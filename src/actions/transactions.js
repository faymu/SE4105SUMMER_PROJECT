import history from '../history'
import { addTransactionToDb, fetchParentTransactions, fetchSitterTransactions } from '../handlers/transactionHandler'

export const addTransaction = (data) => {
  return async (dispatch) => {
    await addTransactionToDb(data)
    dispatch({ type: 'ADD_TRANSACTION' })
  }
}

export const getParentTransactions = () => {
  return async (dispatch) => {
    const transactions = await fetchParentTransactions()
    if (transactions) {
      dispatch({ type: 'GET_PARENT_TRANSACTIONS', payload: transactions })
    }
  }
}

export const getSitterTransactions = () => {
  return async (dispatch) => {
    const transactions = await fetchSitterTransactions()
    if (transactions) {
      dispatch({ type: 'GET_SITTER_TRANSACTIONS', payload: transactions })
    }
  }
}
