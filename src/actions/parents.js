import { addParentToDb, fetchParent } from '../handlers/parentHandler'

export const addParent = (parent) => {
  return async (dispatch) => {
    await addParentToDb(parent)
    dispatch({ type: 'ADD_PARENT', payload: parent })
  }
}

export const getParent = (parentId) => {
  return async (dispatch) => {
    const parent = await fetchParent(parentId)
    if (parent) {
      dispatch({ type: 'GET_PARENT', payload: parent })
    }
  }
}
