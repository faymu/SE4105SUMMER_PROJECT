import { db } from '../../firebase-config'
import swal from 'sweetalert'
import { fetchSitterById } from '../handlers/sitterHandler'
import { getJobPendingRequests, getJobApprovedRequests, confirmRequest, getSitterApprovedRequest, getSitterPendingRequest, deleteRequest, addRequestToDb } from '../handlers/requestHandler'

export const addRequest = (jobId, parentId) => {
  return async (dispatch) => {
    await addRequestToDb(jobId, parentId)
    dispatch({ type: 'ADD_REQUEST' })
  }
}

export const getPendingJobRequests = (jobId) => {
  return async (dispatch) => {
    const requests = await getJobPendingRequests(jobId)
    if (requests) {
      dispatch({ type: 'GET_PENDING_JOB_REQUESTS', payload: requests })
    }
  }
}

export const getApprovedJobRequests = (jobId) => {
  return async (dispatch) => {
    const requests = await getJobApprovedRequests(jobId)
    if (requests) {
      dispatch({ type: 'GET_APPROVED_JOB_REQUESTS', payload: requests })
    }
  }
}

export const confirmRequestAction = (requestId, data) => {
  return async (dispatch) => {
    confirmRequest(requestId, data)
    dispatch({ type: 'CONFIRM_REQUESTS' })
  }
}

export const getPendingRequests = () => {
  return async (dispatch) => {
    const requests = await getSitterPendingRequest()
    if (requests) {
      dispatch({ type: 'GET_SITTER_PENDING_REQUESTS', payload: requests })
    }
  }
}

export const getApprovedRequests = () => {
  return async (dispatch) => {
    const approvedRequests = await getSitterApprovedRequest()
    if (approvedRequests) {
      dispatch({ type: 'GET_SITTER_APPROVED_REQUESTS', payload: approvedRequests })
    }
  }
}

export const declineRequest = (requestId) => {
  return async (dispatch) => {
    await deleteRequest(requestId)
    dispatch({ type: 'DECLINE_REQUESTS' })
  }
}
