import { addSitter, fetchSitters, fetchSitterById } from '../handlers/sitterHandler'

export const getSitters = () => {
  return async (dispatch) => {
    const sitters = await fetchSitters()
    if (sitters) {
      dispatch({ type: 'GET_SITTER', payload: sitters })
    }
  }
}

export const addSitterAction = (sitter) => {
  return async (dispatch) => {
    await addSitter(sitter)
    dispatch({ type: 'ADD_SITTER', payload: sitter })
  }
}

export const getSitterById = (sitterId) => {
  return async (dispatch) => {
    const sitter = await fetchSitterById(sitterId)
    if (sitter) {
      dispatch({ type: 'GET_SITTER_BY_ID', payload: sitter })
    }
  }
}
