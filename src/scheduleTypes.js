export default [
  { value: 'Occasional', text: 'Occasional' },
  { value: 'Regular', text: 'Regular' },
  { value: 'One Time', text: 'One Time' }
]
