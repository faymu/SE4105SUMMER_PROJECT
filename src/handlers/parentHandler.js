import { db, auth } from '../../firebase-config'
import swal from 'sweetalert'
import history from '../history'

export async function addParentToDb(parent) {
  try {
    const authenticate = await auth.createUserWithEmailAndPassword(parent.email, parent.password)
    await db.collection('parents').doc(authenticate.uid).set({
      firstName: parent.firstName,
      lastName: parent.lastName,
      gender: parent.gender,
      birthday: parent.birthday,
      location: parent.location,
      fullAddress: parent.fullAddress,
      contactNumber: parent.contactNumber,
      description: parent.description
    })
    swal({ title: 'Parent Added!', icon: 'success' })
    history.push('/')
  } catch (error) {
    swal({ title: 'Error adding parent', text: error.message, icon: 'error' })
  }
}

export async function fetchParent(parentId) {
  try {
    const parent = await db.collection('parents').doc(parentId).get()
    return { id: parent.id, data: parent.data() }
  } catch (error) {
    swal({ title: 'Error getting parent', text: error.message, icon: 'error' })
  }
}
