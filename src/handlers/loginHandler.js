import { db, auth } from '../../firebase-config'
import swal from 'sweetalert'
import history from '../history'

export async function logInWithEmailAndPassword (user) {
  try {
    const authenticate = await auth.signInWithEmailAndPassword(user.email, user.password)
    const parents = await db.collection('parents').doc(authenticate.uid).get()
    const sitters = await db.collection('sitters').doc(authenticate.uid).get()

    if (authenticate) {
      if (sitters.data() && !parents.data()) {
        swal({ title: 'Successfully logged in!', icon: 'success' })
        localStorage.setItem('userId', sitters.id)
        history.push('/sitter/dashboard')
      } else {
        swal({ title: 'Successfully logged in!', icon: 'success' })
        localStorage.setItem('userId', parents.id)
        history.push('/parent/dashboard')
      }
    }
  } catch (error) {
    swal({ title: 'Error logging in!', text: error.message, icon: 'error' })
  }
}
