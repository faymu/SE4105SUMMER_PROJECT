import { auth } from '../../firebase-config'
import swal from 'sweetalert'
import history from '../history'

export async function userLogOut () {
  try {
    await auth.signOut()
    localStorage.removeItem('userId')
    swal({ title: 'Successfully Logged Out!', icon: 'success' })
    history.push('/login')
  } catch (error) {
    swal({ title: 'Error Logging in ', message: error.message, icon: 'error' })
  }
}
