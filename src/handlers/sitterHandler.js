import { db, auth } from '../../firebase-config'
import swal from 'sweetalert'
import { parse, differenceInCalendarYears, isAfter } from 'date-fns'

export function getAge (birthday) {
  birthday = parse(birthday)
  const dateToday = new Date()
  const age = differenceInCalendarYears(dateToday, birthday)
  return isAfter(birthday, dateToday) ? age : age - 1
}

export async function addSitter (sitter) {
  try {
    const authenticate = await auth.createUserWithEmailAndPassword(sitter.email, sitter.password)
    if (authenticate) {
      await db.collection('sitters').doc(authenticate.uid).set({
        firstName: sitter.firstName,
        lastName: sitter.lastName,
        gender: sitter.gender,
        birthday: sitter.birthday,
        location: sitter.location,
        fullAddress: sitter.fullAddress,
        contactNumber: sitter.contactNumber,
        description: sitter.description
      })
      swal({ title: 'Sitter added!', icon: 'success' })
    }
  } catch (error) {
    swal({ title: 'Error adding sitter', text: error.message, icon: 'error' })
  }
}

export async function fetchSitters () {
  let sittersList = []
  try {
    const sitters = await db.collection('sitters').get()
    sitters.forEach(sitter => {
      sittersList.push({ id: sitter.id, data: sitter.data() })
    })
    return sittersList
  } catch (error) {
    swal({ title: 'Error getting sitter', text: error.message, icon: 'error' })
  }
}

export async function fetchSitterById (sitterId) {
  try {
    const sitter = await db.collection('sitters').doc(sitterId).get()
    return { id: sitter.id, data: sitter.data() }
  } catch (error) {
    swal({ title: 'Error getting sitter', text: error.message, icon: 'error' })
  }
}
