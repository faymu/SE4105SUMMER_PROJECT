import { db } from '../../firebase-config'
import swal from 'sweetalert'
import history from '../history'
import { isPast, isBefore, isToday } from 'date-fns'

export function getRateDetails (rate) {
  let appFee
  rate = parseInt(rate)
  if (rate < 100) {
    return { appFee: 0, totalRate: rate }
  } else {
    appFee = rate * 0.05
    return { appFee: appFee, totalRate: rate - appFee }
  }
}

// const isAgeRange = (input) => {
//   const numbers = input.split('-')
//   if (numbers.length === 2) {
//     if (numbers[0].length > 0 && numbers[1].length > 0) {
//       if (Number(numbers[0]) && Number(numbers[1])) {
//         return true
//       }
//     }
//   }
//   return false
// }

export async function addJobToDb (job) {
  try {
    if ((!isPast(job.startDate) || isToday(job.startDate)) && !isBefore(job.endDate, job.startDate)) {
      await db.collection('jobs').add({
        location: job.location,
        rate: job.rate,
        numOfChildren: job.numOfChildren,
        childAgeRange: job.childAgeRange,
        scheduleType: job.scheduleType,
        startDate: job.startDate,
        endDate: job.endDate,
        description: job.description,
        isUrgent: !job.isUrgent ? false : true,
        parentId: localStorage.getItem('userId')
      })

      swal({ title: 'Job Added!', icon: 'success' })
      history.push('/parent/jobs')
    } else {
      swal({ title: 'Enter correct dates!', icon: 'warning' })
    }
  } catch (error) {
    swal({ title: 'Error in adding job!', text: error.message, icon: 'error' })
  }
}

export async function fetchJobs () {
  let listOfJobs = []
  let parent = []
  try {
    const jobs = await db.collection('jobs').get()
    jobs.forEach(job => {
      listOfJobs.push({ id: job.id, data: job.data() })
    })
    for (const job of listOfJobs) {
      const parentData = await db.collection('parents').doc(job.data.parentId).get()
      parent.push({ id: parentData.id, data: parentData.data() })
    }
    return { jobs: listOfJobs, parent: parent }
  } catch (error) {
    swal({ title: 'Error in getting job!', text: error.message, icon: 'error' })
  }
}

export async function fetchFilteredJobs (filter) {
  let listOfJobs = []
  let parent = []
  try {
    let jobs = await db.collection('jobs')
    let parent = []
    if (filter.location) {
      jobs = await jobs.where('location', '==', filter.location)
    }
    if (filter.scheduleType) {
      jobs = await jobs.where('scheduleType', '==', filter.scheduleType)
    }
    if (filter.rate) {
      jobs = await jobs.where('rate', '==', filter.rate)
    }

    jobs = await jobs.get()
    jobs.forEach(job => {
      listOfJobs.push({ id: job.id, data: job.data() })
    })

    for (const job of listOfJobs) {
      const parentData = await db.collection('parents').doc(job.data.parentId).get()
      parent.push({ id: parentData.id, data: parentData.data() })
    }

    return { jobs: listOfJobs, parent: parent }
  } catch (error) {
    swal({ title: 'Error in filtered job!', text: error.message, icon: 'error' })
  }
}

export async function fetchJobsAddedByUser () {
  let listOfJobs = []
  try {
    let userId = localStorage.getItem('userId')
    const jobs = await db.collection('jobs').where('parentId', '==', userId).get()
    jobs.forEach((job) => {
      listOfJobs.push({ id: job.id, data: job.data() })
    })
    return listOfJobs
  } catch (error) {
    swal({ title: 'Error in getting jobs added by user!', text: error.message, icon: 'error' })
  }
}

export async function deletePostedJob (jobId) {
  try {
    const confirmation = await swal({
      title: 'Are you sure to delete this job?',
      icon: 'warning',
      buttons: true,
      dangerMode: true
    })
    if (confirmation) {
      await db.collection('jobs').doc(jobId).delete()
      swal({ title: 'Job Deleted!', icon: 'success' })
    }
  } catch (error) {
    swal({ title: 'Error deleting job!', text: error.message, icon: 'error' })
  }
}
