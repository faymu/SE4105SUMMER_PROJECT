import { db } from '../../firebase-config'
import swal from 'sweetalert'
import history from '../history'

export async function addRequestToDb (jobId, parentId) {
  let requests
  let sitterId = localStorage.getItem('userId')
  try {
    const confirmation = await swal({
      title: 'Are you sure to submit a request?',
      icon: 'warning',
      buttons: true,
      dangerMode: true
    })
    let request = await db.collection('requests')
      .where('jobId', '==', jobId)
      .where('sitterId', '==', sitterId)
      .get()
    request.forEach((req) => {
      requests = req.data()
    })

    if (confirmation && !requests) {
      await db.collection('requests').add({
        jobId: jobId,
        parentId: parentId,
        sitterId: sitterId,
        status: 'pending'
      })
      swal({ title: 'Request has been added!', icon: 'success' })
    } else {
      swal({ title: 'You already submitted a request for this Job', icon: 'warning' })
    }
  } catch (error) {
    swal({ title: 'Error adding request!', text: error.message, icon: 'error' })
  }
}

export async function confirmRequest (requestId, data) {
  try {
    await db.collection('requests').doc(requestId).update({
      status: 'approved',
      note: data.note
    })
    swal({ title: 'Request confirmed!', icon: 'success' })
    history.push('/parent/jobs')
  } catch (error) {
    swal({ title: 'Error confirming request', text: error.message, icon: 'error' })
  }
}

export async function getSitterApprovedRequest () {
  let requestList = []
  let jobDetails = []
  try {
    const sitterId = localStorage.getItem('userId')
    const requests = await db.collection('requests')
      .where('sitterId', '==', sitterId)
      .where('status', '==', 'approved')
      .get()

    requests.forEach((request) => {
      requestList.push({ id: request.id, data: request.data() })
    })

    for (const request of requestList) {
      const job = await db.collection('jobs').doc(request.data.jobId).get()
      const parent = await db.collection('parents').doc(request.data.parentId).get()
      jobDetails.push({ parentData: parent.data(), data: job.data() })
    }
    return { requests: requestList, job: jobDetails }
  } catch (error) {
    swal({ title: 'Error getting approved request!', text: error.message, icon: 'error' })
  }
}

export async function getSitterPendingRequest () {
  let requestList = []
  let jobDetails = []
  try {
    const sitterId = localStorage.getItem('userId')
    const requests = await db.collection('requests')
      .where('sitterId', '==', sitterId)
      .where('status', '==', 'pending')
      .get()
    requests.forEach((request) => {
      requestList.push({ id: request.id, data: request.data() })
    })

    for (const request of requestList) {
      const job = await db.collection('jobs').doc(request.data.jobId).get()
      const parent = await db.collection('parents').doc(request.data.parentId).get()
      jobDetails.push({ parentData: parent.data(), data: job.data() })
    }
    return { requests: requestList, job: jobDetails }
  } catch (error) {
    swal({ title: 'Error getting pending request!', text: error.message, icon: 'error' })
  }
}

export async function deleteRequest (requestId) {
  try {
    const confirmation = await swal({
      title: 'Are you sure you want to decline?',
      icon: 'warning',
      buttons: true,
      dangerMode: true
    })

    await db.collection('requests').doc(requestId).delete()

    if (confirmation) {
      swal({ title: 'Request has been declined!', icon: 'success' })
    }
  } catch (error) {
    swal({ title: 'Error deleting request', text: error.message, icon: 'error' })
  }
}

export async function getJobPendingRequests (jobId) {
  let requestsList = []
  let sitters = []
  try {
    const requests = await db.collection('requests')
      .where('jobId', '==', jobId)
      .where('status', '==', 'pending')
      .get()

    requests.forEach(async (request) => {
      const data = request.data()
      requestsList.push({ id: request.id, data })
    })

    for (const request of requestsList) {
      let sitter = await db.collection('sitters').doc(request.data.sitterId).get()
      sitters.push({ id: sitter.id, sitter: sitter.data() })
    }
    return { requests: requestsList, sitters: sitters }
  } catch (error) {
    swal({ title: 'Error getting pending job requests', text: error.message, icon: 'error' })
  }
}

export async function getJobApprovedRequests (jobId) {
  let requestsList = []
  let sitters = []
  try {
    const requests = await db.collection('requests')
      .where('jobId', '==', jobId)
      .where('status', '==', 'approved')
      .get()

    requests.forEach(async (request) => {
      const data = request.data()
      requestsList.push({ id: request.id, data })
    })

    for (const request of requestsList) {
      let sitter = await db.collection('sitters').doc(request.data.sitterId).get()
      sitters.push({ id: sitter.id, sitter: sitter.data() })
    }
    return { requests: requestsList, sitters: sitters }
  } catch (error) {
    swal({ title: 'Error getting approved job requests', text: error.message, icon: 'error' })
  }
}
