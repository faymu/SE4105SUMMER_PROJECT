import { db } from '../../firebase-config'
import swal from 'sweetalert'
import history from '../history'
import { getRateDetails } from './jobHandler'

export async function addTransactionToDb (data) {
  let job = await db.collection('jobs').doc(data.jobId).get()
  job = { id: job.id, data: job.data() }
  const rateDetails = getRateDetails(job.data.rate)

  try {
    await db.collection('requests').doc(data.requestId).update({ status: 'done' })
    await db.collection('transactions').add({
      requestId: data.requestId,
      jobId: data.jobId,
      sitterId: data.sitterId,
      parentId: localStorage.getItem('userId'),
      date: new Date().toDateString(),
      startDate: job.data.startDate,
      endDate: job.data.endDate,
      rate: job.data.rate,
      appFee: rateDetails.appFee,
      totalCharge: rateDetails.totalRate
    })
    await db.collection('jobs').doc(data.jobId).delete()
    swal({ title: 'Transaction added!', icon: 'success' })
    history.push('/parent/transactions')
  } catch (error) {
    swal({ title: 'Error in adding transaction!', text: error.message, icon: 'error' })
  }
}

export async function fetchParentTransactions () {
  let transactionDetails = []
  let sitter
  try {
    const transactions = await db.collection('transactions')
      .where('parentId', '==', localStorage.getItem('userId'))
      .get()
    transactions.forEach((transaction) => {
      transactionDetails.push({ id: transaction.id, data: transaction.data() })
    })

    for (const transaction of transactionDetails) {
      sitter = await db.collection('sitters').doc(transaction.data.sitterId).get()
      sitter = { id: sitter.id, data: sitter.data() }
    }
    return { transactions: transactionDetails, sitter }
  } catch (error) {
    swal({ title: 'Error in getting parent transaction!', text: error.message, icon: 'error' })
  }
}

export async function fetchSitterTransactions () {
  let transactionDetails = []
  let sitter
  try {
    const transactions = await db.collection('transactions')
      .where('sitterId', '==', localStorage.getItem('userId'))
      .get()

    transactions.forEach((transaction) => {
      transactionDetails.push({ id: transaction.id, data: transaction.data() })
    })

    for (const transaction of transactionDetails) {
      sitter = await db.collection('sitters').doc(transaction.data.sitterId).get()
      sitter = { id: sitter.id, data: sitter.data() }
    }
    return { transactions: transactionDetails, sitter }
  } catch (error) {
    swal({ title: 'Error in getting sitter transaction!', text: error.message, icon: 'error' })
  }
}
