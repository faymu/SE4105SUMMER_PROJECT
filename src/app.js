import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import 'babel-polyfill'
import AppRouter from './routers/AppRouter'
import store from './store/store'

const app = (
  <Provider store={ store }>
    <AppRouter />
  </Provider>
)

ReactDOM.render(app, document.getElementById('app-container'))
