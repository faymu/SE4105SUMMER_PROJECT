import React from 'react'
import { Button, Form, Segment, Radio, Label, Icon, TextArea, Divider, Checkbox } from 'semantic-ui-react'
import { Field, reduxForm, Link } from 'redux-form'
import InputField from './InputField'
import DropdownField from './DropdownField'
import TextAreaField from './TextAreaField'
import CheckboxField from './CheckboxField'
import provinces from '../../provinces'
import schedTypes from '../../scheduleTypes'

const SignUpForm = (props) => {
  const { handleSubmit } = props
  return (
    <Segment>
      <Form onSubmit={handleSubmit}>
        <Field
          label='Location'
          icon='home'
          placeholder='Location'
          name='location'
          options={provinces}
          component={DropdownField}
        />

        <Field
          label='Rate'
          icon='dollar'
          name='rate'
          type='number'
          placeholder='Rate'
          component={InputField}
        />

        <Field
          label='Urgent'
          icon='warning sign'
          name='isUrgent'
          type='checkbox'
          placeholder="Urgent"
          component={CheckboxField}
        />

        <Field
          label='No. of Children'
          icon='child'
          name='numOfChildren'
          type='number'
          placeholder='No. of Children'
          component={InputField}
        />

        <Field
          label='Children Age Range'
          icon='birthday'
          name='childAgeRange'
          type='text'
          placeholder="Children's Age Range"
          component={InputField}
        />

        <Field
          label='Schedule Type'
          icon='calendar'
          placeholder='Schedule Type'
          name='scheduleType'
          options={schedTypes}
          component={DropdownField}
        />

        <Field
          label='Start Date'
          icon='calendar'
          name='startDate'
          type='date'
          placeholder='Date expected to start'
          component={InputField}
        />

        <Field
          label='End Date'
          icon='calendar'
          name='endDate'
          type='date'
          placeholder='Date expected to end'
          component={InputField}
        />
        <Field
          label='Description'
          icon='content'
          placeholder='Description'
          name='description'
          component={TextAreaField}
        />
        <Divider />
        <Segment textAlign='center'>
          <Button type='submit' color='teal'>Post A Job</Button>
        </Segment>
      </Form>
    </Segment>
  )
}

export default reduxForm({
  form: 'jobform'
})(SignUpForm)
