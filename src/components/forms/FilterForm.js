import React from 'react'
import { Segment, Button, Form, Dropdown, TextArea, Icon, Label, Divider } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import InputField from './InputField'
import DropdownField from './DropdownField'
import '../../styles/Home.css'
import provinces from '../../provinces'
import schedTypes from '../../scheduleTypes'

let FilterForm = (props) => {
  const { handleSubmit } = props
  return (
    <Form onSubmit={handleSubmit} size='large'>
      <Segment stacked width={5}>
        <Field
          label='Location'
          icon='home'
          placeholder='Location'
          name='location'
          options={provinces}
          component={DropdownField}
        />

        <Field
          label='Schedule type'
          icon='calendar'
          placeholder='Schedule Type'
          name='scheduleType'
          options={schedTypes}
          component={DropdownField}
        />

        <Field
          label='Rate'
          icon='dollar'
          name='rate'
          type='number'
          placeholder="Rate"
          component={InputField}
        />
        <Divider />
        <Button color='teal' fluid size='large' type='submit'>Apply</Button>
      </Segment>
    </Form>
  )
}

FilterForm = reduxForm({
  form: 'FilterForme'
})(FilterForm)

export default FilterForm
