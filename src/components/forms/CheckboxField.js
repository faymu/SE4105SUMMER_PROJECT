import React from 'react'
import { Checkbox } from 'semantic-ui-react'

export default (props) => (
  <Checkbox
    label={props.label}
    onChange ={ (e, data) => props.input.onChange(data.checked) }
  />
)
