import React from 'react'
import { Button, Form, Segment, Radio, Container, Divider, Header } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import InputField from './InputField'
import DropdownField from './DropdownField'
import TextAreaField from './TextAreaField'
import provinces from '../../provinces'

const SignUpForm = (props) => {
  const { handleSubmit } = props
  const gender = [{ value: 'Female', text: 'Female' }, { value: 'Male', text: 'Male' }]
  return (
    <Container style={{ width: '500px' }}>
      <Form onSubmit={handleSubmit}>
        <Field
          label='First Name'
          icon='user'
          name='firstName'
          type='text'
          placeholder='First Name'
          component={InputField}
        />

        <Field
          label='Last Name'
          icon='user'
          name='lastName'
          type='text'
          placeholder='Last Name'
          component={InputField}
        />

        <Field
          label='Email'
          icon='mail'
          name='email'
          type='text'
          placeholder='Email address'
          component={InputField}
        />

        <Field
          label='Password'
          icon='lock'
          name='password'
          type='password'
          placeholder='Password'
          component={InputField}
        />

        <Field
          label='Gender'
          icon='intergender'
          name='gender'
          placeholder='Gender'
          options={gender}
          component={DropdownField}
        />

        <Field
          label='Birthday'
          icon='birthday'
          name='birthday'
          type='date'
          placeholder='Birthday'
          component={InputField}
        />

        <Field
          label='Full Address'
          icon='home'
          name='fullAddress'
          type='text'
          placeholder='Full Address'
          component={InputField}
        />

        <Field
          label='Province'
          icon='address card outline'
          placeholder='Location'
          name='location'
          options={provinces}
          component={DropdownField}
        />

        <Field
          label='Contact Number'
          icon='phone'
          name='contactNumber'
          type='number'
          placeholder='Contact Number'
          component={InputField}
        />

        <Field
          label='Tell us more about your self'
          icon='content'
          placeholder='Tell us more about your self'
          name='description'
          component={TextAreaField}
        />
        <Divider />
        <Button type='submit' color='blue' fluid>SignUp</Button>
      </Form>
    </Container >
  )
}

export default reduxForm({
  form: 'signup'
})(SignUpForm)
