import React from 'react'
import { Dropdown, Label, Icon, Divider } from 'semantic-ui-react'

export default ({
  input,
  label,
  icon,
  options,
  placeholder
}) => (
  <div>
    <Label><Icon name={icon} />{label}</Label>
    <Divider fitted hidden/>
    <div>
      <Dropdown
        search selection
        fluid
        onChange={ (e, data) => { input.onChange(data.value) } }
        value={input.value}
        placeholder={placeholder}
        options={options}
      />
      <Divider hidden/>
    </div>
  </div>
)
