import React from 'react'
import { Label, Icon, TextArea, Divider } from 'semantic-ui-react'

export default ({
  input,
  label,
  icon,
  placeholder,
  meta: { touched, error, warning }
}) => (
  <div>
    <Label><Icon name={icon} />{label}</Label>
    <Divider fitted hidden/>
    <div>
      <TextArea
        {...input}
        placeholder={placeholder}
      />

      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
      <Divider hidden/>
    </div>
  </div>
)
