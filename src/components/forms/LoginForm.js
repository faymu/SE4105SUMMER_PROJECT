import React from 'react'
import { Segment, Button, Form, Grid, Image, Divider, Message } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import InputField from './InputField'
import { Link } from 'react-router-dom'

const LoginForm = (props) => {
  const { handleSubmit } = props
  return (
    <Form onSubmit={handleSubmit} size='large'>
      <Segment stacked textAlign='center'>
        <Image src='images/logo.png' size='small' centered as={Link} to='/' />
        <Field
          fluidIcon='user'
          iconPosition='left'
          placeholder='E-mail address'
          type='text'
          name='email'
          component={InputField}
        />
        <Field
          fluidIcon='lock'
          iconPosition='left'
          placeholder='Password'
          type='password'
          name='password'
          component={InputField}
        />
        <Divider hidden />
        <Button
          color='blue'
          className='loginButton'
          fluid size='large'
          type='submit'
          content='Login'
        />
      </Segment>
    </Form>
  )
}

export default reduxForm({
  form: 'LogInForm'
})(LoginForm)
