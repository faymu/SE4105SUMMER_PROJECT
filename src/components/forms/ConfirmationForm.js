import React from 'react'
import { Segment, Button, Form, Dropdown, TextArea, Icon, Label, Divider } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import InputField from './InputField'
import DropdownField from './DropdownField'
import TextAreaField from './TextAreaField'
import provinces from '../../provinces'
import schedTypes from '../../scheduleTypes'
import '../../styles/Home.css'

let ConfirmationForm = (props) => {
  const { handleSubmit } = props
  return (
    <Form onSubmit={handleSubmit} size='large'>
      <Segment stacked>
        <Field
          label='Note'
          icon='sticky note'
          placeholder='Send note to the sitter'
          name='note'
          component={TextAreaField}
        />
        <Divider />
        <Button color='teal' fluid size='large' type='submit'>Confirm</Button>
      </Segment>
    </Form>
  )
}

ConfirmationForm = reduxForm({
  form: 'ConfirmationForm'
})(ConfirmationForm)

export default ConfirmationForm
