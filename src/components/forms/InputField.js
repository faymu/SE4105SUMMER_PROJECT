import React from 'react'
import { Input, Label, Icon, Divider, Container } from 'semantic-ui-react'

export default ({
  input,
  label,
  icon,
  fluidIcon,
  iconPosition,
  type,
  placeholder,
  meta: { touched, error, warning }
}) => (
  <div>
    {label ? <Label><Icon name={icon}/>{label}</Label> : <Divider hidden/>}
    <Divider fitted hidden />
    <div>
      <Input
        iconPosition={iconPosition}
        fluid icon={fluidIcon}
        {...input}
        type={type}
        placeholder={placeholder}
      />

      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
    <Divider hidden/>
  </div>
)
