import React from 'react'
import ParentMenu from './ParentMenu'
import { Button, List, Segment, Grid, Dropdown, Header, Item, Label, Icon, Divider, Container, Breadcrumb } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { getRateDetails } from '../../handlers/jobHandler'

class Jobs extends React.Component {
  componentDidMount () {
    this.props.getUserAddedJobs()
  }

  render () {
    return (
      <Container>
        <ParentMenu />
        <Segment
          inverted color='blue'
          textAlign='center'
          size='massive'>
          Jobs Posted
        </Segment>
        <Container>
          <Segment textAlign='center'>
            <Button basic color='blue' as={Link} to='/parent/addjob'><Icon name='briefcase' />Post Job</Button>
            <Divider />
            <Breadcrumb>
              <Breadcrumb.Section active>Jobs Posted</Breadcrumb.Section>
              <Breadcrumb.Divider icon='right chevron' />
              <Breadcrumb.Section >Requests</Breadcrumb.Section>
            </Breadcrumb>
          </Segment>
          <Item.Group divided>
            {this.props.userAddedJobs && this.props.userAddedJobs.map((job, index) => {
              let rateDetails = getRateDetails(job.data.rate)
              return (<Item key={index}>
                <Item.Image src='images/jobIcon2.png' />
                <Item.Content>
                  <Item.Header>{job.data.scheduleType}</Item.Header>
                  <Item.Meta>
                    <span className='cinema'>{job.data.isUrgent === true ? 'Urgent' : 'Nonurgent'}</span>
                  </Item.Meta>
                  <Grid>
                    <Grid.Row>
                      <Icon name='point' />{job.data.location}
                    </Grid.Row>
                    <Grid.Column textAlign='center' width={4}>
                      <Icon name='tag' /> Rate
                      <Divider hidden fitted />
                      ₱ {job.data.rate}
                    </Grid.Column>
                    <Grid.Column width={4} textAlign='center'>
                      <Icon name='percent' /> 5% fee
                      <Divider hidden fitted />
                      {rateDetails.appFee}
                    </Grid.Column>
                    <Grid.Column width={4} textAlign='center'>
                      <Icon name='dollar' /> Total Rate
                      <Divider hidden fitted />
                      ₱ {rateDetails.totalRate}
                    </Grid.Column>
                  </Grid>
                  <Divider />
                  <Item.Meta>
                    <span className='cinema'>Details</span>
                  </Item.Meta>
                  <Item.Description><i>{job.data.description}</i></Item.Description>
                  <Item.Description>Number of Children to Sit: {job.data.numOfChildren}</Item.Description>
                  <Item.Description>Age Range: {job.data.childAgeRange}</Item.Description>
                  <Item.Description>From {job.data.startDate} to {job.data.endDate}</Item.Description>
                  <Item.Extra>
                    <Button color='red' floated='right' onClick={ () => { this.props.deleteJob(job.id) } }>
                      <Icon name='trash' /> Delete Job
                    </Button>
                    <Button as={Link} to={`/parent/job/${job.id}`} primary floated='right'>
                      Requests <Icon name='right chevron' />
                    </Button>
                    <Divider clearing />
                  </Item.Extra>
                </Item.Content>
              </Item>)
            }
            )}

          </Item.Group>

        </Container >
      </Container>
    )
  }
}

export default Jobs
