import React from 'react'
import { Button, List, Icon, Grid, Modal, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import ConfirmationForm from '../forms/ConfirmationForm'
import { confirmRequestAction } from '../../actions/requests'

const ConfirmationModal = (props) => (
  <Modal
    trigger={<Button color='green'>Approve</Button>}
    closeIcon
    closeOnDimmerClick={true}
    size='small'
  >
    <Segment textAlign='center' inverted color='teal'>
      <Icon name='check circle' /> APPROVE REQUEST CONFIRMATION
    </Segment>
    <Modal.Content>
      <ConfirmationForm onSubmit={(values, dispatch) => { dispatch(confirmRequestAction(props.requestId, values)) } } />
    </Modal.Content>
  </Modal>
)

export default ConfirmationModal
