import React from 'react'
import SignUpForm from '../forms/SignUpForm'
import { Segment, Button, Container, Divider, Header } from 'semantic-ui-react'
import { addParent } from '../../actions/parents'
import { Link } from 'react-router-dom'

const submit = (values, dispatch) => {
  dispatch(addParent(values))
}

const ParentSignUpPage = () => (
  <Container style={{ width: '500px' }}>>
    <Divider />
    <Header textAlign='center'>
      Sign Up as Parent
      </Header>
    <Segment stacked>
      <SignUpForm onSubmit={submit} />
      <Divider />
      <Segment textAlign='center'>
      <Button color='grey' content='Back' as={Link} to='/' size='medium' fluid />
      </Segment>
    </Segment>
  </Container>
)

export default ParentSignUpPage
