import React from 'react'
import ParentMenu from './ParentMenu'
import { Header, Image, Button, Card, Icon, Segment, Container } from 'semantic-ui-react'
import { getAge } from '../../handlers/sitterHandler'

class ParentDashboard extends React.Component {
  componentDidMount () {
    this.props.getSitters()
  }

  render () {
    let sitters
    if (this.props.sitters) {
      sitters = this.props.sitters
    }

    return (
      <Container>
        <ParentMenu />
        <Segment
          inverted color='blue'
          textAlign='center'
          size='massive'>
          Hello! Welcome dear parent!
        </Segment>
        <Container>
          <Card.Group>
            {sitters && sitters.map((sitter, index) => {
              sitter = sitter.data
              return (
                <Card color='blue' key={index}>
                  <Card.Content>
                    <Image floated='left' size='tiny' src='images/sitterIcon.png' />
                    <Card.Header>
                      {sitter.firstName}{sitter.lastName}
                    </Card.Header>
                    <Card.Meta>
                      {sitter.gender}, {getAge(sitter.birthday)}
                    </Card.Meta>
                    <Icon name='point' /> {sitter.fullAddress}
                    <br />
                    <Icon name='call' /> {sitter.contactNumber}
                  </Card.Content>
                </Card>
              )
            })}
          </Card.Group>
        </Container>
      </Container>
    )
  }
}

export default ParentDashboard
