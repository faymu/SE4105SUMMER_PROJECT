import React from 'react'
import { Header, Image, Segment, Button } from 'semantic-ui-react'
import { connect, dispatch } from 'react-redux'
import { userLogOut } from '../../handlers/logoutHandler'

class ParentHeader extends React.Component {
  componentDidMount () {
    const parentId = localStorage.getItem('userId')
    this.props.getParent(parentId)
  }

  render () {
    let parentName
    if (this.props.parent) {
      parentName = this.props.parent.data.firstName
    }
    return (
      <div>
        <Header as='h1' floated='left'>
          <Image src='images/banner.png' />
        </Header>
        <Header as='h5' floated='right'>
          <Image circular src='images/parentUserIcon.png' />
          <Header.Content>
            {parentName}
            <Header.Subheader>
              <a onClick={() => { userLogOut() }}>Logout </a>
            </Header.Subheader>
          </Header.Content>
        </Header>
      </div>
    )
  }
}

export default ParentHeader
