import React from 'react'
import { Menu, Divider } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import ParentHeader from '../../containers/ParentHeaderContainer'

const ParentMenu = (props) => (
  <div>
    <ParentHeader />
    <Divider fitted clearing hidden />
    <Menu pointing secondary>
      <Menu.Item color='blue' name='dashboard' as={NavLink} to='/parent/dashboard' activeClassName='active item' />
      <Menu.Item color='blue' name='job board' as={NavLink} to='/parent/jobs' activeClassName='active item' />
      <Menu.Item color='blue' name='transactions' as={NavLink} to='/parent/transactions' activeClassName='active item' />
    </Menu >
  </div>
)

export default ParentMenu
