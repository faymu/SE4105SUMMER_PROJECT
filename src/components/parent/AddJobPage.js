import React from 'react'
import JobForm from '../forms/JobForm'
import { addJob } from '../../actions/jobs'
import { Link } from 'react-router'
import { Container, Segment, Grid, Divider, Button } from 'semantic-ui-react'

const submit = (values, dispatch) => {
  dispatch(addJob(values))
}

const AddJobPage = () => (
  <div>
    <Segment textAlign='center' inverted color='teal'>
      <h1>POST A JOB</h1>
    </Segment>
    <Grid centered={true}>
      <Grid.Column width='10'>
        <JobForm onSubmit={submit} />
      </Grid.Column>
    </Grid>
  </div>
)

export default AddJobPage
