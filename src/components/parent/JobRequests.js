import React from 'react'
import { Button, Segment, Breadcrumb, Container, Card, Image, Icon, Divider, Grid } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import ConfirmationModal from './ConfirmationModal'
import ParentMenu from './ParentMenu'
import { getAge } from '../../handlers/sitterHandler'
import '../../styles/JobRequests.css'

class JobRequests extends React.Component {
  componentDidMount () {
    this.props.getPendingRequests(this.props.match.params.jobId)
    this.props.getApprovedRequests(this.props.match.params.jobId)
  }

  render () {
    return (
      <Container>
        <ParentMenu />
        <Segment
          inverted color='blue'
          textAlign='center'
          size='massive'>
          Jobs Posted
        </Segment>
        <Container>
          <Segment textAlign='center'>
            <Breadcrumb>
              <Breadcrumb.Section as={Link} to='/parent/jobs'>Jobs Posted</Breadcrumb.Section>
              <Breadcrumb.Divider icon='right arrow' />
              <Breadcrumb.Section active>Requests</Breadcrumb.Section>
            </Breadcrumb>
          </Segment>
          <Grid centered>
            <Grid.Column textAlign='center' width={8}>
              <h3 className='pendingRequestLabel'><Icon name='clock' />PENDING REQUESTS</h3>
              <Divider />
              <Card.Group>
                {this.props.pendingRequests ? this.props.pendingRequests.requests.map((request, index) => {
                  let sittersPendingRequest = this.props.pendingRequests.sitters
                  return (<Card key={index}>
                    <Card.Content>
                      <Image floated='right' size='mini' src='/images/sitterUserIcon.png' />
                      <Card.Header>{sittersPendingRequest[index].sitter.firstName}{sittersPendingRequest[index].sitter.lastName}</Card.Header>
                      <Card.Meta>{sittersPendingRequest[index].sitter.gender},{getAge(sittersPendingRequest[index].sitter.birthday)} </Card.Meta>
                      <Card.Description><Icon name='point' />{sittersPendingRequest[index].sitter.fullAddress}</Card.Description>
                      <Card.Description><Icon name='call' />{sittersPendingRequest[index].sitter.contactNumber}</Card.Description>
                      <Divider />
                      <Segment textAlign='center'>
                        <Card.Description>{sittersPendingRequest[index].sitter.description}</Card.Description>
                      </Segment>
                    </Card.Content>
                    <Card.Content extra>
                      <div className='ui two buttons'>
                        <ConfirmationModal requestId={request.id} />
                        <Button color='red' content='Decline' onClick={() => { this.props.declineRequest(request.id) }} />
                      </div>
                    </Card.Content>
                  </Card>
                  )
                }) : ''}
              </Card.Group>
            </Grid.Column>
            <Grid.Column textAlign='center' width={8}>
              <h3 className='approvedRequestLabel'><Icon name='check circle outline' />APPROVED REQUESTS</h3>
              <Divider />
              <Card.Group>
                {this.props.approvedRequests ? this.props.approvedRequests.requests.map((request, index) => {
                  let sittersApprovedRequest = this.props.approvedRequests.sitters
                  return (<Card key={index}>
                    <Card.Content>
                      <Image floated='right' size='mini' src='/images/sitterUserIcon.png' />
                      <Card.Header>{sittersApprovedRequest[index].sitter.firstName}{sittersApprovedRequest[index].sitter.lastName}</Card.Header>
                      <Card.Meta>{sittersApprovedRequest[index].sitter.gender},{getAge(sittersApprovedRequest[index].sitter.birthday)} </Card.Meta>
                      <Card.Description><Icon name='point' />{sittersApprovedRequest[index].sitter.fullAddress}</Card.Description>
                      <Card.Description><Icon name='call' />{sittersApprovedRequest[index].sitter.contactNumber}</Card.Description>
                      <Divider />
                      <Segment textAlign='center'>
                        <Card.Description>{sittersApprovedRequest[index].sitter.description}</Card.Description>
                      </Segment>
                    </Card.Content>
                    <Card.Content extra>
                      <div className='ui two buttons'>
                        <Button color='blue' content='Done' onClick={() => {
                          this.props.addTransaction({
                            requestId: request.id,
                            jobId: this.props.match.params.jobId,
                            sitterId: sittersApprovedRequest[index].id
                          })
                        }} />
                      </div>
                    </Card.Content>
                  </Card>
                  )
                }) : ''}
              </Card.Group>
            </Grid.Column>
          </Grid>
        </Container>
      </Container>
    )
  }
}

export default JobRequests
