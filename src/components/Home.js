import React from 'react'
import { Image } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

const Home = () => (
  <div>
    <Image as={Link} to='/login' src='images/home.png' fluid wrapped centered verticalAlign='middle'/>
  </div>
)

export default Home
