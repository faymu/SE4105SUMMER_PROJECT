import React from 'react'
import { Image, Message, Grid, Container, Divider } from 'semantic-ui-react'
import LoginForm from './forms/LoginForm'
import { logIn } from '../actions/login'
import SignUpChoiceModal from './SignUpChoiceModal'
import '../styles/Home.css'

const submit = (values, dispatch) => {
  dispatch(logIn(values))
}

const LoginPage = () => (<Grid centered={true}>
  <Grid.Column width='8'>
    <LoginForm onSubmit={submit} />
    <Message className='message' color='blue'>
      <p>Don't have an account yet?</p>
      <SignUpChoiceModal />
    </Message>
  </Grid.Column>
</Grid>
)

export default LoginPage
