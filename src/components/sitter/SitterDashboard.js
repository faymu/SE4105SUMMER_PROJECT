import React from 'react'
import { Button, List, Segment, Grid, Container, Card, Icon } from 'semantic-ui-react'
import SitterMenu from './SitterMenu'
import provinces from '../../provinces'
import FilterModal from './FilterModal'
import JobData from './Job'
import { connect, dispatch } from 'react-redux'
import JobDetails from './JobDetails'

class SitterDashboard extends React.Component {
  componentDidMount () {
    this.props.getJobs()
  }

  render () {
    let jobs
    let parent
    if (this.props.jobData) {
      jobs = this.props.jobData.jobs
      parent = this.props.jobData.parent
    }
    return (
      <Container>
        <SitterMenu />
        <Segment
          inverted color='green'
          textAlign='center'
          size='massive'>
          Hello! Welcome dear sitter!
        </Segment>
        <Segment textAlign='center'>
          <FilterModal />
        </Segment>
        <Container>
          <Card.Group>
            {jobs && parent && jobs.map((job, index) => (
              <Card color='green' key={index}>
                <JobData job={job.data} />
                <Card.Content extra>
                  <Icon name='user circle outline' /> Posted by {parent[index].data.firstName}
                </Card.Content>
                <Card.Content>
                  <Button.Group widths='2'>
                    <JobDetails job={job.data} parent={parent[index].data} />
                    <Button
                      icon='star'
                      color='green'
                      content='Express Interest'
                      onClick={() => { this.props.addRequest(job.id, job.data.parentId) }}
                    />
                  </Button.Group>
                </Card.Content>
              </Card>
            )
            )}
          </Card.Group>
        </Container>
      </Container>
    )
  }
}

export default (SitterDashboard)
