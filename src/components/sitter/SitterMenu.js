import React from 'react'
import { Menu, Divider } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import SitterHeader from '../../containers/SitterHeaderContainer'

const SitterMenu = (props) => (
  <div>
    <SitterHeader />
    <Divider fitted clearing hidden />
    <Menu pointing secondary>
      <Menu.Item color='green' name='dashboard' as={NavLink} to='/sitter/dashboard' activeClassName='active item' />
      <Menu.Item color='green' name='my bookings' as={NavLink} to='/sitter/bookings' activeClassName='active item' />
      <Menu.Item color='green' name='transactions' as={NavLink} to='/sitter/transactions' activeClassName='active item' />
    </Menu >
  </div>
)

export default SitterMenu
