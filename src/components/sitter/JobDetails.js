import React from 'react'
import { Button, Header, Segment, Icon, Modal, Image, Divider, Grid, Item, Container } from 'semantic-ui-react'
import { getRateDetails } from '../../handlers/jobHandler'

const JobDetails = (props) => {
  let rateDetails = getRateDetails(props.job.rate)
  return (
    <Modal
      trigger={<Button color='blue'><Icon name='info circle' />Details</Button>}
      closeIcon
      closeOnDimmerClick={true}
      size='tiny'
    >
      <Container>
        <Divider />
        <Header textAlign='center'>
          {props.job.scheduleType} Sitter
        </Header>
        <Modal.Content>
          <Grid centered>
            <Grid.Row>
              <Icon name='marker' />Location: {props.job.location}
            </Grid.Row>
            <Divider />
            <Grid.Row width={16} textAlign='center'>
              <Grid.Column textAlign='center' width={5}>
                <Icon name='tag' /> Rate
                <Divider hidden fitted />
                Php {props.job.rate}
              </Grid.Column>
              <Grid.Column width={5} textAlign='center'>
                <Icon name='percent' /> App fee
                <Divider hidden fitted />
                5% of {rateDetails.appFee}
              </Grid.Column>
              <Grid.Column width={5} textAlign='center'>
                <Icon name='dollar' /> Total Rate
                <Divider hidden fitted />
                *Additional Php 50/day
              Php {rateDetails.totalRate}
              </Grid.Column>
            </Grid.Row>
            <Divider />
            <Grid.Row>
              Details: {props.job.description}
            </Grid.Row>
            <Grid.Row>
              Number of Children to Sit: {props.job.numOfChildren}
              <br />
              Starting Date: {props.job.startDate}
            </Grid.Row>
          </Grid>
          <Segment textAlign='center'>
            Posted by:
            <Item>
              <Item.Image size='mini' src='images/parenticon2.gif' />
              <Item.Content verticalAlign='middle'>
                <Item.Header>
                  {props.parent.firstName} {props.parent.lastName}
                </Item.Header>
                {props.parent.contactNumber}
                <br />
                {props.parent.fullAddress}
              </Item.Content>
            </Item>
          </Segment>
        </Modal.Content>
      </Container>
    </Modal >
  )
}

export default JobDetails
