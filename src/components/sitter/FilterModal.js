import React from 'react'
import { Button, Header, Icon, Modal, Image, Segment, Grid, Container } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import FilterForm from '../forms/FilterForm'
import { getFilteredJobs } from '../../actions/jobs'
const submit = (values, dispatch) => {
  dispatch(getFilteredJobs(values))
}

const FilterModal = () => (
  <Container>
    <Modal
      trigger={<Button basic color='green'><Icon name='options' />Filter</Button>}
      closeIcon
      closeOnDimmerClick={true}
      size='small'
    >
      <Segment size='tiny' textAlign='center' inverted color='teal'>
        <h1>Filter</h1>
      </Segment>
      <Modal.Content>
        <Grid centered={true}>
          <Grid.Column width='10'>
            <FilterForm onSubmit={submit} />
          </Grid.Column>
        </Grid>
      </Modal.Content>
    </Modal>
  </Container>
)

export default FilterModal
