import React from 'react'
import SitterMenu from './SitterMenu'
import { format, differenceInDays } from 'date-fns'
import { Container, Divider, Segment, Card, Icon, Button } from 'semantic-ui-react'
import Receipt from '../Receipt'

class Transactions extends React.Component {
  componentDidMount () {
    this.props.getTransactions()
  }

  render () {
    return (
      <Container>
        <SitterMenu />
        <Segment
          inverted color='green'
          textAlign='center'
          size='massive'>
          My Transactions
        </Segment>
        <Container>
          <Card.Group centered>
            {this.props.transactions ? this.props.transactions.transactions.map((transaction, index) => {
              const sitter = this.props.transactions.sitter
              return (
                <Card key={index}>
                  <Card.Content>
                    <Card.Header>{format(transaction.data.date, 'MM/DD/YYYY')}</Card.Header>
                    <Card.Meta>{sitter.data.firstName}</Card.Meta>
                    <Card.Description>
                      Start Date: {transaction.data.startDate}
                      <Divider fitted hidden/>
                      End Date: {transaction.data.endDate} ({differenceInDays(transaction.data.startDate, transaction.data.endDate)} days)
                    </Card.Description>
                    <Divider fitted hidden/>
                    <Receipt id={transaction.id} transaction={transaction.data} sitter={sitter.data}/>
                  </Card.Content>
                </Card>
              )
            }) : ''}
          </Card.Group>
        </Container>
      </Container>
    )
  }
}

export default Transactions
