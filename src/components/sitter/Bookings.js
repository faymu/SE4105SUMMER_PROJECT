import React from 'react'
import { Grid, Segment, Container, Feed, Icon, Divider, Image, Card } from 'semantic-ui-react'
import SitterMenu from './SitterMenu'
import { getRateDetails } from '../../handlers/jobHandler'
import '../../styles/Bookings.css'

class Bookings extends React.Component {
  componentDidMount () {
    this.props.getPendingRequests()
    this.props.getApprovedRequests()
  }

  render () {
    let pendingRequests
    let approvedRequests
    let jobPending
    let jobApproved
    if (this.props.pendingRequests && this.props.approvedRequests) {
      pendingRequests = this.props.pendingRequests.requests
      approvedRequests = this.props.approvedRequests.requests
      jobPending = this.props.pendingRequests.job
      jobApproved = this.props.approvedRequests.job
    }
    return (
      <Container>
        <SitterMenu />
        <Segment
          inverted color='green'
          textAlign='center'
          size='massive'>
          My Bookings
        </Segment>
        <Grid textAlign='center'>
          <Grid.Column width={5}>
            <Segment padded>
              <h3 className='pendingRequestLabel'><Icon name='clock' />PENDING REQUESTS</h3>
              <Divider />
              {pendingRequests && pendingRequests.map((request, index) => (
                <Feed key={index}>
                  <Feed.Event>
                    <Feed.Label>
                      <Image src='images/jobicon.png' />
                    </Feed.Label>
                    <Feed.Content>
                      <Feed.Summary>
                        <Icon name='point' />{jobPending[index].data.location}
                        <br />
                        <Feed.Date>From {jobPending[index].data.startDate} to {jobPending[index].data.startDate}</Feed.Date>
                      </Feed.Summary>
                      <Icon name='time' />{jobPending[index].data.scheduleType}
                      <br />
                      <Feed.Extra text>
                        <Icon name='dollar' />Php {jobPending[index].data.rate}
                      </Feed.Extra>
                      <Feed.Meta>
                        <Icon name='user circle outline' />{jobPending[index].parentData.firstName} {jobPending[index].parentData.lastName}
                      </Feed.Meta>
                    </Feed.Content>
                  </Feed.Event>
                  <Divider />
                </Feed>
              ))}
            </Segment>
          </Grid.Column>
          <Grid.Column width={11}>
            <Segment padded>
              <h3 className='approvedRequestLabel'><Icon name='check circle outline' />APPROVED REQUESTS</h3>
              <Divider />
              <Card.Group>
                {approvedRequests && approvedRequests.map((request, index) => {
                  let rateDetails = getRateDetails(jobApproved[index].data.rate)
                  return (<Card key={index}>
                    <Card.Header>
                      <Icon name='point' />{jobApproved[index].data.location} -
                      <Icon name='time' />{jobApproved[index].data.scheduleType}
                    </Card.Header>
                    <Card.Content description={request.data.note} />
                    <Card.Content>
                      <Grid>
                        <Grid.Row width={16} textAlign='center'>
                          <Grid.Column textAlign='center' width={5}>
                            <Icon name='tag' /> Rate
                            <Divider hidden fitted />
                            ₱ {jobApproved[index].data.rate}
                          </Grid.Column>
                          <Grid.Column width={5} textAlign='center'>
                            <Icon name='percent' /> App fee
                            <Divider hidden fitted />
                            5% of {rateDetails.appFee}
                          </Grid.Column>
                          <Grid.Column width={5} textAlign='center'>
                            <Icon name='dollar' /> Total Rate
                            <Divider hidden fitted />
                            ₱ {rateDetails.totalRate}
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Card.Content>
                    <Card.Content>
                      From {jobApproved[index].data.startDate} until {jobApproved[index].data.endDate}
                    </Card.Content>
                    <Card.Content extra>
                      <Icon name='user circle outline' />
                      {jobApproved[index].parentData.firstName} {jobApproved[index].parentData.lastName}
                    </Card.Content>
                  </Card>
                  )
                })}
              </Card.Group>
            </Segment>
          </Grid.Column>
        </Grid>
      </Container>
    )
  }
}

export default Bookings
