import React from 'react'
import { Header, Image, Segment } from 'semantic-ui-react'
import { userLogOut } from '../../handlers/logoutHandler'

class SitterHeader extends React.Component {
  componentDidMount () {
    const sitterId = localStorage.getItem('userId')
    this.props.getSitter(sitterId)
  }

  render () {
    let sitterName
    if (this.props.sitter) {
      sitterName = this.props.sitter.data.firstName
    }
    return (
      <div>
        <Header as='h1' floated='left'>
          <Image src='images/banner.png' />
        </Header>
        <Header as='h5' floated='right'>
          <Image circular src='images/sitterUserIcon.png' />
          <Header.Content>
            {sitterName}
            <Header.Subheader>
              <a onClick={() => { userLogOut() }}>Logout </a>
            </Header.Subheader>
          </Header.Content>
        </Header>
      </div>
    )
  }
}

export default SitterHeader
