import React from 'react'
import { Card, Button, Link, Image } from 'semantic-ui-react'
import { dispatch } from 'react-redux'

const Job = (props) => (
  <Card.Content>
    <Image floated='right' size='tiny' src='images/jobicon.png'/>
    <Card.Header>
      {props.job.scheduleType}
    </Card.Header>
    <Card.Meta>
      {props.job.isUrgent === true ? 'Urgent' : 'Nonurgent'}
    </Card.Meta>
    Location: {props.job.location}
    <br />
    Rate: Php {props.job.rate}
    <br />
    From {props.job.startDate} to {props.job.endDate}
  </Card.Content>
)

export default Job
