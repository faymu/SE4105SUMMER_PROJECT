import React from 'react'
import SignUpForm from '../forms/SignUpForm'
import { Segment, Button, Container, Divider, Header } from 'semantic-ui-react'
import { addSitterAction } from '../../actions/sitters'
import { Link } from 'react-router-dom'

const submit = (values, dispatch) => {
  dispatch(addSitterAction(values))
}

const SitterSignUpPage = () => (
  <Container style={{ width: '500px' }}>>
    <Divider />
    <Header textAlign='center'>
      Sign Up as Sitter
    </Header>
    <Segment stacked>
      <SignUpForm onSubmit={submit} />
      <Divider />
      <Segment textAlign='center'>
        <Button color='grey' content='Back' as={Link} to='/' size='medium' fluid />
      </Segment>
    </Segment>
  </Container>
)

export default SitterSignUpPage
