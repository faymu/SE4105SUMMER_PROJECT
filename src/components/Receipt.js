import React from 'react'
import { Container, Button, List, Icon, Grid, Modal, Header, Image, Divider, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

const Receipt = (props) => (
  <Modal
    trigger={<Button basic color='blue'><Icon name='file text outline' />View Receipt</Button>}
    closeIcon
    closeOnDimmerClick={true}
    size='mini'
  >
    <Container>
      <Segment padded>
        <Modal.Content>
          <Image size='small' centered src='images/logo.png' />
          <Grid centered>
            <Grid.Row>
              {props.transaction.date}
            </Grid.Row>
            <Grid.Row color='blue'>
              <h1>₱{props.transaction.totalCharge}</h1>
            </Grid.Row>
            <Segment attached inverted color='green'>
              <b>{props.sitter.firstName} {props.sitter.lastName}</b>
              <Divider fitted hidden />
              {props.sitter.fullAddress}
              <Divider fitted hidden />
              {props.sitter.contactNumber}
            </Segment>
            <Grid.Row width={8} textAlign='center'>
              <Grid.Column textAlign='center' width={5}>
                <b>Rate</b>
                <Divider fitted hidden />
                ₱{props.transaction.rate}
              </Grid.Column>
              <Grid.Column width={5} textAlign='center'>
                <b>App Fee</b>
                <Divider fitted hidden />
                ₱{props.transaction.appFee}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Modal.Content>
      </Segment>
    </Container>
  </Modal>
)

export default Receipt
