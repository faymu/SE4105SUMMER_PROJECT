import React from 'react'
import { Container, Segment, Image, Divider } from 'semantic-ui-react'

const Error404 = () => (
  <Container>
    <Segment textAlign='center'>
      Error 404 Page Not Found!
      <Divider />
      <Image src='images/404.png' />
    </Segment>
  </Container>
)

export default Error404
