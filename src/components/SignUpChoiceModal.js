import React from 'react'
import { Button, Header, Icon, Modal, Image, Grid, Divider } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
const SignUpChoiceModal = () => (
  <Modal
    trigger={<Button basic color='blue'>Sign Up</Button>}
    closeIcon
    closeOnDimmerClick={true}
    size='small'
  >
    <Divider/>
    <Image centered size='small' src='images/logo.png' /><br />
    <Header textAlign='center'>
      Sign Up
    </Header>
    <Modal.Content>
      <Button.Group fluid>
        <Button size='large' color='blue' as={Link} to='sitter/signup'>SITTER</Button >
        <Button size='large' color='green' as={Link} to='parent/signup'>PARENT</Button>
      </Button.Group>
    </Modal.Content>
  </Modal >
)

export default SignUpChoiceModal
