import React from 'react'
import { connect } from 'react-redux'
import { getSitterById } from '../actions/sitters'
import Header from '../components/sitter/SitterHeader'

export const mapStateToProps = (state) => {
  return {
    sitter: state.sitters.sitter
  }
}

export const mapDispatchToProps = dispatch => ({
  getSitter: (sitterId) => dispatch(getSitterById(sitterId))
})

const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(Header)

export default HeaderContainer
