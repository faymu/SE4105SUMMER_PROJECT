import React from 'react'
import { connect } from 'react-redux'
import { getPendingJobRequests, getApprovedJobRequests, declineRequest } from '../actions/requests'
import { addTransaction } from '../actions/transactions'
import JobRequests from '../components/parent/JobRequests'

export const mapStateToProps = (state) => {
  return {
    pendingRequests: state.requests.pendingJobRequests,
    approvedRequests: state.requests.approvedJobRequests
  }
}

export const mapDispatchToProps = dispatch => ({
  getPendingRequests: (jobId) => { dispatch(getPendingJobRequests(jobId)) },
  getApprovedRequests: (jobId) => { dispatch(getApprovedJobRequests(jobId)) },
  declineRequest: (requestId) => { dispatch(declineRequest(requestId)) },
  addTransaction: (data) => { dispatch(addTransaction(data)) }
})

const RequestContainer = connect(mapStateToProps, mapDispatchToProps)(JobRequests)

export default RequestContainer
