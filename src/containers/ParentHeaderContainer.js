import React from 'react'
import { connect } from 'react-redux'
import { getParent } from '../actions/parents'
import Header from '../components/parent/ParentHeader'

export const mapStateToProps = (state) => {
  return {
    parent: state.parents.parent
  }
}

export const mapDispatchToProps = dispatch => ({
  getParent: (parentId) => dispatch(getParent(parentId))
})

const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(Header)

export default HeaderContainer
