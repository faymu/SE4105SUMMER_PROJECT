import React from 'react'
import { connect } from 'react-redux'
import { getAllJobs } from '../actions/jobs'
import { addRequest } from '../actions/requests'
import SitterDashboard from '../components/sitter/SitterDashboard'

export const mapStateToProps = (state) => {
  return {
    jobData: state.jobs.jobs
  }
}

export const mapDispatchToProps = dispatch => ({
  getJobs: () => dispatch(getAllJobs()),
  addRequest: (jobId, parentId) => dispatch(addRequest(jobId, parentId))
})

const JobContainer = connect(mapStateToProps, mapDispatchToProps)(SitterDashboard)

export default JobContainer
