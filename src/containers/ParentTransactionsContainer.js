import React from 'react'
import { connect } from 'react-redux'
import { getParentTransactions } from '../actions/transactions'
import ParentTransactions from '../components/parent/Transactions'

export const mapStateToProps = (state) => {
  return {
    transactions: state.transactions.parentTransactions
  }
}

export const mapDispatchToProps = dispatch => ({
  getTransactions: () => dispatch(getParentTransactions())
})

const ParentTransactionContainer = connect(mapStateToProps, mapDispatchToProps)(ParentTransactions)

export default ParentTransactionContainer
