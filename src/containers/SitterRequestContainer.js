import React from 'react'
import { connect } from 'react-redux'
import { getApprovedRequests, getPendingRequests } from '../actions/requests'
import Bookings from '../components/sitter/Bookings'

export const mapStateToProps = (state) => {
  return {
    pendingRequests: state.requests.pendingSitterRequests,
    approvedRequests: state.requests.approvedSitterRequests
  }
}

export const mapDispatchToProps = dispatch => ({
  getPendingRequests: () => dispatch(getPendingRequests()),
  getApprovedRequests: () => dispatch(getApprovedRequests())
})

const SitterRequestContainer = connect(mapStateToProps, mapDispatchToProps)(Bookings)

export default SitterRequestContainer
