import React from 'react'
import { connect } from 'react-redux'
import { getSitterTransactions } from '../actions/transactions'
import SitterTransactions from '../components/sitter/Transactions'

export const mapStateToProps = (state) => {
  return {
    transactions: state.transactions.sitterTransactions
  }
}

export const mapDispatchToProps = dispatch => ({
  getTransactions: () => dispatch(getSitterTransactions())
})

const SitterTransactionContainer = connect(mapStateToProps, mapDispatchToProps)(SitterTransactions)

export default SitterTransactionContainer
