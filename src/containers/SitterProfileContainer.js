import React from 'react'
import { connect } from 'react-redux'
import { getSitterById } from '../actions/sitters'
import SitterProfile from '../components/parent/SitterProfile'

export const mapStateToProps = (state) => {
  return {
    requests: state.sitters.sitter
  }
}

export const mapDispatchToProps = dispatch => ({
  getSitterById: (sitterId) => { dispatch(getSitterById(sitterId)) }
})

const SitterContainer = connect(mapStateToProps, mapDispatchToProps)(SitterProfile)

export default SitterContainer
