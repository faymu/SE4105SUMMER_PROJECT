import React from 'react'
import { connect } from 'react-redux'
import { getUserAddedJobs, deleteJob } from '../actions/jobs'
import Jobs from '../components/parent/Jobs'
export const mapStateToProps = (state) => {
  return { userAddedJobs: state.jobs.userAddedJobs }
}

export const mapDispatchToProps = dispatch => ({
  getUserAddedJobs: () => dispatch(getUserAddedJobs()),
  deleteJob: (jobId) => { dispatch(deleteJob(jobId)) }
})

const UserAddedJobsContainer = connect(mapStateToProps, mapDispatchToProps)(Jobs)

export default UserAddedJobsContainer
