import React from 'react'
import { connect } from 'react-redux'
import { getSitters } from '../actions/sitters'
import ParentDashboard from '../components/parent/ParentDashboard'

export const mapStateToProps = (state) => {
  return { sitters: state.sitters.sitters }
}

export const mapDispatchToProps = dispatch => ({
  getSitters: () => dispatch(getSitters())
})

const ParentContainer = connect(mapStateToProps, mapDispatchToProps)(ParentDashboard)

export default ParentContainer
